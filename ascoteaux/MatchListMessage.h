//
//  MatchListMessage.h
//  ascoteaux
//
//  Created by Arnaud on 30/09/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONProtocole.h"

@interface MatchListMessage : NSObject <JSONProtocole>
@property (nonatomic, strong) NSArray * matchs;

@end
