//
//  MenuCell.m
//  ascoteaux
//
//  Created by Arnaud on 31/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import "MenuCell.h"

@implementation MenuCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
