//
//  AppDelegate.h
//  ascoteaux
//
//  Created by Arnaud on 02/09/2015.
//  Copyright (c) 2015 Arnaud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UIWindow *window;
@property BOOL isLogged;
@property (nonatomic, strong) NSString *notificationType;
@property (nonatomic, strong) UINavigationController * currentNavController;
@end

