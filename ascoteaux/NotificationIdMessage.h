//
//  NotificationIdMessage.h
//  ascoteaux
//
//  Created by Arnaud on 04/11/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONProtocole.h"

@interface NotificationIdMessage : NSObject <JSONProtocole>

@property (nonatomic,strong) NSString * notificationId;

@end
