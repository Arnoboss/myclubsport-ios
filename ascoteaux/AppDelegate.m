//
//  AppDelegate.m
//  ascoteaux
//
//  Created by Arnaud on 02/09/2015.
//  Copyright (c) 2015 Arnaud. All rights reserved.
//

#import "AppDelegate.h"
#import "Constant.h"
#import "AFNetworking.h"
#import "Constant.h"
#import "NotificationIdMessage.h"
#import <AudioToolbox/AudioToolbox.h>
#import "LiveTableVC.h"
#import "NewsTableVC.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    //on regarde si l'utilisateur est loggué et si le token est toujours valable
    if ([[NSUserDefaults standardUserDefaults] stringForKey:TOKEN_ID]){
        self.isLogged=true;
        // TODO eventuellement checker la date du token. Il faut qu'elle soit < 3 mois
        //NSString * tokenExpirationDate=[[NSUserDefaults standardUserDefaults] stringForKey:TOKEN_EXPIRATION_DATE];
        //NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        //[dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        //NSDate *date = [dateFormat dateFromString:tokenExpirationDate];
        //NSDate *today = [NSDate date];
    }
    
    [UIApplication sharedApplication].applicationIconBadgeNumber=0;
    
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
    #ifdef __IPHONE_8_0
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeAlert) categories:nil];
        [application registerUserNotificationSettings:settings];
    #endif
    } else {
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge;
        [application registerForRemoteNotificationTypes:myTypes];
    }
    
    // Override point for customization after application launch.
    return YES;
}

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}
#endif

//On récupère le token pour les notifications push. S'il est différent de l'ancien, on l'envoie au serveur
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    NSString * oldNotifToken = [[NSUserDefaults standardUserDefaults] stringForKey:NOTIFICATION_TOKEN];
    const unsigned *tokenBytes = [deviceToken bytes];
    NSString *newNotifToken = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                          ntohl(tokenBytes[0]), ntohl(tokenBytes[1]), ntohl(tokenBytes[2]),
                          ntohl(tokenBytes[3]), ntohl(tokenBytes[4]), ntohl(tokenBytes[5]),
                          ntohl(tokenBytes[6]), ntohl(tokenBytes[7])];
    NSLog(@"Notification token: %@", newNotifToken);
    
    if (!oldNotifToken || (![oldNotifToken isEqualToString:newNotifToken])){
        NSMutableDictionary *requestMsg = [[NSMutableDictionary alloc] init];
        NotificationIdMessage *notificationMsg = [[NotificationIdMessage alloc] init];
        notificationMsg.notificationId = newNotifToken;
        
        [requestMsg setObject:[notificationMsg toNSDictionary] forKey:@"payLoad"];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/plain"];
        AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"content-type"];
        [requestSerializer setStringEncoding:NSUTF8StringEncoding];
        manager.requestSerializer = requestSerializer;
        
        [manager PUT:[BASE_URL stringByAppendingString: NOTIFICATIONS_URL] parameters:requestMsg success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
        
    }
}

//En cas d'erreur lors de la récupération du token
-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    NSLog(@"JSON: %@", @"errorNotif");
    
}

/**
 * Méthode permettant de gérer la réception d'une notification
 * push lorsque l'application est active.
 * La méthode reçoit en entrée le flux JSON constituant la
 * notification Push.
 */
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)notificationInfo {
    NSLog(@"remote notification: %@",[notificationInfo description]);
    // Récupération du payload
    NSDictionary *apsInfo = [notificationInfo objectForKey:@"aps"];
    
    // Récupération du dictionnaire décrivant l'alerte
    NSDictionary *alert = [apsInfo objectForKey:@"alert"];
    NSLog(@"Received Push Alert: %@", alert);
    
    // On fera vibrer le terminal
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    
    // On incrémente le badge de 1.
    NSString *badge = [apsInfo objectForKey:@"badge"];
    NSLog(@"Received Push Badge: %@", badge);
    [UIApplication sharedApplication].applicationIconBadgeNumber=1;
    
    // On récupère le texte à afficher dans le body de l'alert
    NSString* message = [notificationInfo objectForKey:@"body"];
    NSString* title = [notificationInfo objectForKey:@"title"];
    self.notificationType = [notificationInfo objectForKey:@"type"];
    
    // On récupère le libellé du bouton de droite
    NSString* libelleButton = NSLocalizedString(@"display", @"Afficher");
    
    // On teste si l'état de l'application est actif ou pas
    if (application.applicationState == UIApplicationStateActive) {
        // L'application est en cours d'utilisation on affiche la popup
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                            message:message
                                                           delegate:self
                                                  cancelButtonTitle:@"Annuler"
                                                  otherButtonTitles:libelleButton,nil];
        alertView.alertViewStyle = UIAlertViewStyleDefault;
        [alertView show];
    } else {
        // Si l'application tourne en tâche de fond on redirige directement
        // vers l'écran d'affichage du message.
        // On se dirige vers l'affichage du message à partir de notre tabBar.
        [self displayNotificationDetails];
    }
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self displayNotificationDetails];
}


- (void) displayNotificationDetails{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if ([self.notificationType isEqualToString:@"live"]){
        UINavigationController *navController = self.currentNavController;
        LiveTableVC *lvc = [storyboard instantiateViewControllerWithIdentifier:@"LiveTableVC"];
        [navController.visibleViewController.navigationController pushViewController:lvc animated:YES];
    }
    else{
        UINavigationController *navController = self.currentNavController;
        NewsTableVC *nvc = [storyboard instantiateViewControllerWithIdentifier:@"NewsTableVC"];
        [navController.visibleViewController.navigationController pushViewController:nvc animated:YES];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
