//
//  NewsTableVC.h
//  ascoteaux
//
//  Created by Arnaud on 14/09/2015.
//  Copyright (c) 2015 Arnaud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsListMessage.h"
#import "ASCoteauxTableViewController.h"

@interface NewsTableVC : ASCoteauxTableViewController

@property (nonatomic, strong)  NewsListMessage *newsList; 
@end
