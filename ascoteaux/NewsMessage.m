//
//  NewsMessage.m
//  ascoteaux
//
//  Created by Arnaud on 17/09/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import "NewsMessage.h"

@implementation NewsMessage

-(id) initWithDictionary: (NSDictionary *) dictionary{
    self = [super init];
    if (self) {
        self.content = [dictionary objectForKey: @"content"];
        
        NSTimeInterval timestamp = (NSTimeInterval)[[dictionary objectForKey: @"date"] doubleValue];
        
        self.date = [NSDate dateWithTimeIntervalSince1970:timestamp/1000];
        self.imageBlobKey = [dictionary objectForKey: @"imageBlobKey"];
        self.imageUrl = [dictionary objectForKey: @"imageUrl"];
        self.title = [dictionary objectForKey: @"title"];
    }
    return self;
}


/**
 Utilisé pour convertir un objet seul en Json. Pas testé mais devrait être fonctionnel si l'objet contient des objets de base (NSNumber, NSString, ...)
 */
-(NSString *) convertToJSON{
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;

}

/**
 Convertit l'objet en dictionnaire. Obligatoire pour le convertir en JSON s'il est dans un Objet complexe (ex: NewsListMessage)
 */
- (NSMutableDictionary *)toNSDictionary
{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setValue:self.content forKey:@"content"];
    [dictionary setValue:self.date forKey:@"date"];
    [dictionary setValue:self.imageBlobKey forKey:@"imageBlobKey"];
    [dictionary setValue:self.imageUrl forKey:@"imageUrl"];
    [dictionary setValue:self.title forKey:@"title"];
    
    return dictionary;
}

- (NSString *) getFormatedDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSLocale *frLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"fr_FR"];
    [formatter setLocale:frLocale];

    [formatter setDateStyle:NSDateFormatterLongStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    return [formatter stringFromDate:self.date];
}

@end
