//
//  NewsTableVC.m
//  ascoteaux
//
//  Created by Arnaud on 14/09/2015.
//  Copyright (c) 2015 Arnaud. All rights reserved.
//

#import "NewsTableVC.h"
#import "AFNetworking.h"
#import "Constant.h"
#import "NewsViewCell.h"
#import "NewsMessage.h"
#import "NewsDetailsViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface NewsTableVC ()

@end

@implementation NewsTableVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //initialisation de la navigation bar
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.title = NSLocalizedString(@"news", @"news");
    
    [self getDataFromServer: nil];
}


//retrieves news list from server
-(void) getDataFromServer: (UIRefreshControl *) refresh{
    
    UIActivityIndicatorView *activityIndicator;
    if (!refresh){
         activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0);
        [self.view addSubview: activityIndicator];
        
        [activityIndicator startAnimating];
    }
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/plain"];
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"content-type"];
    manager.requestSerializer = requestSerializer;
    
    [manager GET:[BASE_URL stringByAppendingString: NEWS_URL] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        self.newsList = [[NewsListMessage alloc] initWithDictionary:[responseObject objectForKey: @"payLoad"]];
        //NSLog(@"JSON: %@", [self.newsList convertToJSON]);
        [self.tableView reloadData];
        if (refresh){
            [refresh endRefreshing];
            NSLog(@"End pull to refresh");
        }
        else
            [activityIndicator stopAnimating];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (refresh)
            [refresh endRefreshing];
        [activityIndicator stopAnimating];
        [Util displayPopupRequestProblem];
        NSLog(@"Error: %@", error);
    }];
    
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of sections.
    if (self.newsList)
        return [self.newsList.news count];
    else return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NewsViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"newsCellId" forIndexPath:indexPath];
    
    NewsMessage *news = [[NewsMessage alloc] init];
    news =  [self.newsList.news objectAtIndex:indexPath.row];
    
    cell.lblDate.text = [news getFormatedDate];
    cell.lblTitle.text = news.title;
    
    // Loading images
    cell.ivImage.image = nil; // or cell.poster.image = [UIImage imageNamed:@"placeholder.png"];
    
    [cell.ivImage sd_setImageWithURL:[NSURL URLWithString:news.imageUrl]
                      placeholderImage:[UIImage imageNamed:news.imageUrl]];
    
    return cell;
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"newsDetailsSegue"])
    {
        // Get reference to the destination view controller
        NewsDetailsViewController *vc = [segue destinationViewController];
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        // Pass any objects to the view controller here, like...
        [vc setNews:[self.newsList.news objectAtIndex: indexPath.row]];
    }
}

@end
