//
//  LiveViewCell.h
//  ascoteaux
//
//  Created by Arnaud on 02/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LiveViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *lblDate;
@property (nonatomic, strong) IBOutlet UILabel *lblTitle;
@property (nonatomic, strong) IBOutlet UILabel *lblTeam1;
@property (nonatomic, strong) IBOutlet UILabel *lblTeam2;
@property (nonatomic, strong) IBOutlet UILabel *lblScore;
@property (nonatomic, strong) IBOutlet UILabel *lblStatus;

@end
