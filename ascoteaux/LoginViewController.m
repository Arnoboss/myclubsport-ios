//
//  LoginViewController.m
//  ascoteaux
//
//  Created by Arnaud on 28/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import "LoginViewController.h"
#import "AFNetworking.h"
#import "Constant.h"
#import "LoginMessage.h"
#import "AppDelegate.h"
@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //initialisation de la navigation bar
    self.title = NSLocalizedString(@"login", @"login");
    
    //ajout contour bouton
    self.btnValidate.layer.borderColor = [UIColor blackColor].CGColor;
    self.btnValidate.layer.borderWidth = 0.8;
    self.btnValidate.layer.cornerRadius = 8;
    
    [self.btnValidate addTarget:self action:@selector(validate:)
               forControlEvents:UIControlEventTouchUpInside];
    
}

- (void) validate: (id) sender{
    if ((self.tfLogin.text ==nil) ||(self.tfPassword.text ==nil)|| ([self.tfLogin.text isEqualToString:@""]) ||([self.tfPassword.text isEqualToString:@""])){
        [self displayErrorMessage:NSLocalizedString(@"loginOrPwdEmpty", @"Login or password empty. Please retry")];
    }
    else{
        UIActivityIndicatorView *activityIndicator;
        activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0);
        [self.view addSubview: activityIndicator];
        
        [activityIndicator startAnimating];
        
        LoginMessage *loginMessage = [[LoginMessage alloc] init];
        loginMessage.login = self.tfLogin.text;
        loginMessage.password = self.tfPassword.text;
        
        NSString *oldToken= [[NSUserDefaults standardUserDefaults] stringForKey:OLD_TOKEN_ID];
        if (oldToken){
            SecurityTokenMessage * token= [[SecurityTokenMessage alloc] init];
            token.tokenId = oldToken;
            loginMessage.token = token;
        }
        
        NSMutableDictionary *requestMsg= [[NSMutableDictionary alloc] init];
        
        [requestMsg setObject:[loginMessage toNSDictionary] forKey:@"payLoad"];
        //[requestMsg setObject:@"" forKey:@"applicationVersion"];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/plain"];
        AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"content-type"];
        manager.requestSerializer = requestSerializer;
        
        
        [manager POST:[BASE_URL stringByAppendingString: LOGIN_URL] parameters:requestMsg success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [activityIndicator stopAnimating];
            
            NSLog(@"JSON: %@", responseObject);
            if ([responseObject objectForKey:@"errorCode"]){
                [self displayErrorMessage: [responseObject objectForKey:@"exceptionMessage"]];
            }
            else{
                LoginMessage *response= [[LoginMessage alloc] initWithDictionary:[responseObject objectForKey:@"payLoad"]];
                
                //sauvegarde des donnees dans les prefs
                [[NSUserDefaults standardUserDefaults] setValue:response.login forKey:LOGIN];
                [[NSUserDefaults standardUserDefaults] setValue:response.token.tokenId forKey:TOKEN_ID];
                [[NSUserDefaults standardUserDefaults] setValue:response.token.expirationDate forKey:TOKEN_EXPIRATION_DATE];
                
                //On se met en "Logged"
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                appDelegate.isLogged = TRUE;
                
                [self.navigationController popToRootViewControllerAnimated:YES];
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *vc =[storyboard instantiateInitialViewController];
                [self presentViewController:vc animated:YES completion:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [activityIndicator stopAnimating];
            [Util displayPopupRequestProblem];
            NSLog(@"Error: %@", error);
        }];
    }
    
}

@end
