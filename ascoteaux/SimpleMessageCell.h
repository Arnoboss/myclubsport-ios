//
//  SimpleMessageCell.h
//  ascoteaux
//
//  Created by Arnaud on 11/11/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimpleMessageCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *lblMsg;

@end
