//
//  AddCommentViewController.m
//  ascoteaux
//
//  Created by Arnaud on 08/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import "AddCommentViewController.h"
#import "AFNetworking.h"
#import "Constant.h"
#import "Util.h"

@implementation AddCommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.match.commentsList=nil;
    
    //initialisation des composants. En cas de modification d'un commentaire, les champs sont pré-remplis
    if ((self.comment) && (self.comment.id!=0)){
        self.isEditing=TRUE;
        self.title=NSLocalizedString(@"modifyComment", @"modifyComment");
        self.tvComment.text = self.comment.content;
        self.lblMinuteEvent.text = [NSString stringWithFormat:@"%@",self.comment.minuteEvent];
        [self.swIsImportant setOn: self.comment.important];
    }
    else{
        self.isEditing=FALSE;
        self.comment= [[CommentMessage alloc] init];
        self.title=NSLocalizedString(@"addComment", @"addComment");
    }
    
    if ([self.match.status isEqualToString :@"NOT_STARTED"])
        [self.pvStatus selectRow:0 inComponent:0 animated:FALSE];
    
    else if ([self.match.status isEqualToString :@"IN_PROGRESS"])
        [self.pvStatus selectRow:1 inComponent:0 animated:FALSE];
    
    else if ([self.match.status isEqualToString :@"FINISHED"])
        [self.pvStatus selectRow:2 inComponent:0 animated:FALSE];
    
    self.lblTeam1.text = self.match.team1;
    self.lblTeam2.text = self.match.team2;
    self.lblGoalTeam1.text = [NSString stringWithFormat:@"%@",self.match.goalsTeam1];
    self.lblGoalTeam2.text = [NSString stringWithFormat:@"%@",self.match.goalsTeam2];
    self.lblButeursTeam1.tag = 1;
    self.lblButeursTeam2.tag = 2;
    self.lblButeursTeam1.text = self.match.buteursTeam1;
    self.lblButeursTeam2.text = self.match.buteursTeam2;
    
    self.lblGoalTeam2.text = [NSString stringWithFormat:@"%@",self.match.goalsTeam2];
    self.statusArray  = [[NSArray alloc] initWithObjects:@"A venir",@"En cours",@"Terminé", nil];
    
    //ajout contour bouton
    self.btnValidate.layer.borderColor = [UIColor blackColor].CGColor;
    self.btnValidate.layer.borderWidth = 0.8;
    self.btnValidate.layer.cornerRadius = 8;
    
    [self.btnValidate addTarget:self action:@selector(validate:)
               forControlEvents:UIControlEventTouchUpInside];
    
    
    self.lblCommentPlaceHolder.hidden = ([self.tvComment.text length] > 0);
    
}

- (IBAction)backGroundTouched:(id)sender
{
    [self.view endEditing:YES];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

#pragma place holder "comments"
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.lblCommentPlaceHolder.hidden = YES;
}

- (void)textViewDidChange:(UITextView *)txtView
{
    self.lblCommentPlaceHolder.hidden = ([txtView.text length] > 0);
}

- (void)textViewDidEndEditing:(UITextView *)txtView
{
    self.lblCommentPlaceHolder.hidden = ([txtView.text length] > 0);
}


#pragma textfield size
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    if ((textField.tag==1) || (textField.tag==2)){
        return true;
    }
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 2;
}


#pragma PickerView delegate methods

// returns the number of 'columns' to display.

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

// returns the # of rows in each component..

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component{
    return 3;
}



- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row   inComponent:(NSInteger)component{
    switch(row){
        case 0:
            self.match.status=@"NOT_STARTED";
            break;
        case 1:
            self.match.status=@"IN_PROGRESS";
            break;
        case 2:
            self.match.status=@"FINISHED";
            break;
    }
}


//Customisation du pickerview pour que la font soit plus grosse
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* tView = (UILabel*)view;
    if (!tView)
    {
        tView = [[UILabel alloc] init];
        tView.textAlignment = NSTextAlignmentCenter;
        [tView setFont:[UIFont fontWithName:@"System" size:13]];
        tView.numberOfLines=1;
    }
    // Fill the label text here
    tView.text=[self.statusArray objectAtIndex:row];
    return tView;
}


#pragma Action after validation

-(void) validate: (id) sender{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"confirmComment","Confirmer commentaire")
                                                    message:NSLocalizedString(@"confirmCommentMsg","Êtes-vous sûr de vouloir envoyer ce commentaire ?")
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"cancel","Annuler")
                                          otherButtonTitles:NSLocalizedString(@"confirm","Valider"),nil];
    [alert show];
    
}


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        if ([self checkDataIntegrity]){
            [self fillMatchData];
            [self postNewComment];
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"requestErrorTitle", @"Erreur")
                                                            message:NSLocalizedString(@"dataIntegrityError", @"dataIntegrityError")
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
}


- (BOOL) checkDataIntegrity{
    BOOL isOK=TRUE;
    if (self.tvComment.text.length==0)
        isOK=FALSE;
    if (self.lblMinuteEvent.text.length==0)
        isOK=FALSE;
    if (self.lblGoalTeam1.text.length==0)
        isOK=FALSE;
    if (self.lblGoalTeam2.text.length==0)
        isOK=FALSE;
    return isOK;
}


// Fill the comment before post data
- (void) fillMatchData{
    self.comment.content = self.tvComment.text;
    self.comment.minuteEvent = [NSNumber numberWithInt: [self.lblMinuteEvent.text integerValue]];
    self.comment.important = self.swIsImportant.isOn;
    self.comment.notify = self.swNotify.isOn;
    self.match.buteursTeam1 = self.lblButeursTeam1.text;
    self.match.buteursTeam2 = self.lblButeursTeam2.text;
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    
    self.match.goalsTeam1 = [f numberFromString:self.lblGoalTeam1.text];
    self.match.goalsTeam2 = [f numberFromString:self.lblGoalTeam2.text];
    CommentListMessage *commentList = [[CommentListMessage alloc] initWithComment: self.comment];
    self.match.commentsList = commentList;
}

- (void) postNewComment{
    UIActivityIndicatorView *activityIndicator;
    activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0);
    [self.view addSubview: activityIndicator];
    
    [activityIndicator startAnimating];
    
    NSMutableDictionary *requestMsg= [[NSMutableDictionary alloc] init];
    [requestMsg setObject:[self.match toNSDictionary] forKey:@"payLoad"];
    
    [requestMsg setObject: [[NSUserDefaults standardUserDefaults] stringForKey:TOKEN_ID] forKey:TOKEN_ID];
    [requestMsg setObject:[[NSUserDefaults standardUserDefaults] stringForKey:LOGIN] forKey:LOGIN];
    //[requestMsg setObject:@"" forKey:@"applicationVersion"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/plain"];
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    [requestSerializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"content-type"];
    [requestSerializer setStringEncoding:NSUTF8StringEncoding];
    
    manager.requestSerializer = requestSerializer;
    
    //En cas d'édition d'un commentaire, le protocole HTTP utilisé est PUT. En cas d'ajout, on utilise POST
    if (self.isEditing){
        [manager PUT:[BASE_URL stringByAppendingString: LIVE_MATCH_COMMENTS_URL] parameters:requestMsg success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [activityIndicator stopAnimating];
            [self.navigationController popViewControllerAnimated:YES];
            NSLog(@"JSON: %@", responseObject);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [activityIndicator stopAnimating];
            [Util displayPopupRequestProblem];
            NSLog(@"Error: %@", error);
        }];
    }
    else{
        
        [manager POST:[BASE_URL stringByAppendingString: LIVE_MATCH_COMMENTS_URL] parameters:requestMsg success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [activityIndicator stopAnimating];
            [self.navigationController popViewControllerAnimated:YES];
            NSLog(@"JSON: %@", responseObject);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [activityIndicator stopAnimating];
            [Util displayPopupRequestProblem];
            NSLog(@"Error: %@", error);
        }];
    }
    
}



@end