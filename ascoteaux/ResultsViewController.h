//
//  ResultsViewController.h
//  ascoteaux
//
//  Created by Arnaud on 27/09/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASCoteauxViewController.h"

@interface ResultsViewController : ASCoteauxViewController

@property(nonatomic,strong) IBOutlet UIWebView *webView;
@end
