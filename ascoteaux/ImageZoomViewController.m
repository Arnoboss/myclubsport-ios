//
//  ImageZoomViewController.m
//  ascoteaux
//
//  Created by Arnaud on 27/09/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import "ImageZoomViewController.h"

@implementation ImageZoomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.ivImage.image = self.image;
}

@end
