//
//  LiveDetailsViewController.h
//  ascoteaux
//
//  Created by Arnaud on 02/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MatchMessage.h"

@interface LiveDetailsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) MatchMessage * match;

@property (nonatomic, strong) IBOutlet UITableView *tableViewComments;
@property (nonatomic, strong) IBOutlet UILabel *lblDate;
@property (nonatomic, strong) IBOutlet UILabel *lblTeam1;
@property (nonatomic, strong) IBOutlet UILabel *lblTeam2;
@property (nonatomic, strong) IBOutlet UILabel *lblScore;
@property (nonatomic, strong) IBOutlet UILabel *lblButeursTeam1;
@property (nonatomic, strong) IBOutlet UILabel *lblButeursTeam2;

@end
