//
//  StatsViewCell.h
//  ascoteaux
//
//  Created by Arnaud on 24/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatsViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *lblName;
@property (nonatomic, strong) IBOutlet UILabel *lblPlayerInfos;
@property (nonatomic, strong) IBOutlet UILabel *lblGamesPlayed;
@property (nonatomic, strong) IBOutlet UILabel *lblGoals;
@property (nonatomic, strong) IBOutlet UILabel *lblAssists;
@property (nonatomic, strong) IBOutlet UILabel *lblYellowCards;
@property (nonatomic, strong) IBOutlet UILabel *lblRedCards;

@end
