//
//  CommentMessage.h
//  ascoteaux
//
//  Created by Arnaud on 30/09/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONProtocole.h"

@interface CommentMessage : NSObject <JSONProtocole>

@property (nonatomic, strong) NSNumber *id;
@property (nonatomic, strong) NSString *content;
@property (nonatomic) BOOL important;
@property (nonatomic) BOOL notify;
@property (nonatomic, strong) NSNumber *matchId;
@property (nonatomic, strong) NSNumber *minuteEvent;

@end
