//
//  Util.h
//  ascoteaux
//
//  Created by Arnaud on 30/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Util : NSObject

+ (BOOL)connected;
+ (void) displayPopupRequestProblem;
+ (void) disconnect;
@end
