//
//  CommentMessage.m
//  ascoteaux
//
//  Created by Arnaud on 30/09/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import "CommentMessage.h"

@implementation CommentMessage

-(id) initWithDictionary: (NSDictionary *) dictionary{
    self = [super init];
    if (self) {
        self.id = [dictionary objectForKey: @"id"];
        self.content = [dictionary objectForKey: @"content"];
        self.important = [[dictionary objectForKey: @"important"] boolValue];
        self.notify = [[dictionary objectForKey: @"notify"] boolValue];
        self.matchId = [dictionary objectForKey: @"matchId"];
        self.minuteEvent = [dictionary objectForKey: @"minuteEvent"];    }
    return self;
}


/**
 Utilisé pour convertir un objet seul en Json. Pas testé mais devrait être fonctionnel si l'objet contient des objets de base (NSNumber, NSString, ...)
 */
-(NSString *) convertToJSON{
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
    
}

/**
 Convertit l'objet en dictionnaire. Obligatoire pour le convertir en JSON s'il est dans un Objet complexe (ex: NewsListMessage)
 */
- (NSMutableDictionary *)toNSDictionary
{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setValue:self.id forKey:@"id"];
    [dictionary setValue:self.content forKey:@"content"];
    [dictionary setValue: [NSNumber numberWithBool:self.important] forKey:@"important"];
    [dictionary setValue: [NSNumber numberWithBool:self.notify] forKey:@"notify"];
    [dictionary setValue:self.matchId forKey:@"matchId"];
    [dictionary setValue:self.minuteEvent forKey:@"minuteEvent"];
    
    return dictionary;
}


@end
