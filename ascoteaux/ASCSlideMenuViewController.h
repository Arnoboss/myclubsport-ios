//
//  ASCSlideMenuViewController.h
//  ascoteaux
//
//  Created by Arnaud on 13/09/2015.
//  Copyright (c) 2015 Arnaud. All rights reserved.
//

#import "AMSlideMenuMainViewController.h"

@interface ASCSlideMenuViewController : AMSlideMenuMainViewController <UIAlertViewDelegate>

@end
