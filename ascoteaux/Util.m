//
//  Util.m
//  ascoteaux
//
//  Created by Arnaud on 30/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import "Util.h"
#import "AFNetworking.h"
#import "Constant.h"
#import "AppDelegate.h"

@implementation Util

+ (BOOL)connected
{
    return [AFNetworkReachabilityManager sharedManager].reachable;
}


+ (void) displayPopupRequestProblem{

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"requestErrorTitle", @"requestErrorTitle")
                                                    message:NSLocalizedString(@"requestErrorContent", @"requestErrorContent")
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}


+ (void) disconnect{
    //On enregistre le token courant dans old_token
    NSString *oldToken = [[NSUserDefaults standardUserDefaults] stringForKey:TOKEN_ID];
    
    [[NSUserDefaults standardUserDefaults] setValue:oldToken forKey:OLD_TOKEN_ID];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:LOGIN];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:TOKEN_ID];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:TOKEN_EXPIRATION_DATE];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.isLogged = FALSE;
}

@end
