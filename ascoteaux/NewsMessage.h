//
//  NewsMessage.h
//  ascoteaux
//
//  Created by Arnaud on 17/09/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONProtocole.h"

@interface NewsMessage : NSObject <JSONProtocole>

    @property (nonatomic, strong) NSString *content;
    @property (nonatomic, strong) NSDate *date;
    @property (nonatomic, strong) NSString *imageBlobKey;
    @property (nonatomic, strong) NSString *imageUrl;
    @property (nonatomic, strong) NSString *title;

- (NSString *) getFormatedDate;
@end
