//
//  PagingCalendarContentVC.h
//  ascoteaux
//
//  Created by Arnaud on 21/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PagingCalendarContentVC : UIViewController

@property NSUInteger pageIndex;
@property NSString *teamTitle;
@property NSString *url;

@property(nonatomic, weak) IBOutlet UIWebView *webView;


@end
