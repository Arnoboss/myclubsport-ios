//
//  SecurityTokenMessage.h
//  ascoteaux
//
//  Created by Arnaud on 28/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONProtocole.h"

@interface SecurityTokenMessage : NSObject <JSONProtocole>

@property (nonatomic, strong) NSString *tokenId;
@property (nonatomic, strong) NSString *expirationDate;
@property (nonatomic ) BOOL active;

@end
