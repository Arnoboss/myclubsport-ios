//
//  JSONProtocole.h
//  ascoteaux
//
//  Created by Arnaud on 30/09/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#ifndef JSONProtocol_h
#define JSONProtocol_h

@protocol JSONProtocole

- (id) initWithDictionary: (NSDictionary *) dictionary;
- (NSString *) convertToJSON;
- (NSMutableDictionary *)toNSDictionary;

@end
#endif /* JSONProtocol_h */
