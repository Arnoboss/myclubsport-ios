//
//  LiveTableVC.m
//  ascoteaux
//
//  Created by Arnaud on 30/09/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import "LiveTableVC.h"
#import "LiveDetailsViewController.h"
#import "AFNetworking.h"
#import "Constant.h"
#import "Util.h"
#import "LiveViewCell.h"
#import "AppDelegate.h"
#import "SimpleMessageCell.h"

@interface LiveTableVC ()

@end

@implementation LiveTableVC

- (void)viewDidLoad {
    [super viewDidLoad];
    //initialisation de la navigation bar
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.title = NSLocalizedString(@"live", @"live");
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.requestDone = FALSE;
    [self getDataFromServer: nil];
}

-(void) getDataFromServer: (UIRefreshControl *) refresh{
    
    UIActivityIndicatorView *activityIndicator;
    if (!refresh){
        activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0);
        [self.view addSubview: activityIndicator];
        
        [activityIndicator startAnimating];
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/plain"];
    [manager GET:[BASE_URL stringByAppendingString: LIVE_MATCH_URL] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        self.requestDone=TRUE;
        NSLog(@"JSON: %@", responseObject);
        self.matchList = [[MatchListMessage alloc] initWithDictionary:[responseObject objectForKey: @"payLoad"]];
        //NSLog(@"JSON: %@", [self.newsList convertToJSON]);
        [self.tableView reloadData];
        if (refresh){
            [refresh endRefreshing];
        }
        else
            [activityIndicator stopAnimating];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        self.requestDone = TRUE;
        if (refresh)
            [refresh endRefreshing];
        [activityIndicator stopAnimating];
        [Util displayPopupRequestProblem];
        NSLog(@"Error: %@", error);
    }];
    
}


#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of sections.
    if (self.matchList.matchs.count>0)
        return [self.matchList.matchs count];
    else if (self.requestDone) return 1;
    else return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.matchList.matchs == nil || self.matchList.matchs.count == 0){
        SimpleMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"noMatchCell"];
        if (cell == nil) {
            cell = [[SimpleMessageCell alloc] init];
        }
        
        cell.lblMsg.text = NSLocalizedString(@"noLive", @"No live match planned.");
        return cell;
    }
    else{
        LiveViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"matchCellId" forIndexPath:indexPath];
        
        MatchMessage *match = [[MatchMessage alloc] init];
        match =  [self.matchList.matchs objectAtIndex:indexPath.row];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd.MM.YY HH:mm:ss"];
        
        //Optionally for time zone conversions
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"Europe/Paris"]];
        
        cell.lblDate.text = [match getFormatedDate];
        cell.lblTitle.text = match.intitule;
        cell.lblTeam1.text= match.team1;
        cell.lblTeam2.text= match.team2;
        if ([match.status isEqualToString:@"NOT_STARTED"]){
            [cell.lblStatus addConstraint:[NSLayoutConstraint constraintWithItem:cell.lblStatus attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0]];
            cell.lblScore.text = [match getStartMatchTime];
        }
        else {
            cell.lblScore.text= [match getFormatedScore];
            if ([match.status isEqualToString:@"IN_PROGRESS"]){
                cell.lblStatus.text= NSLocalizedString(@"in_progress", @"in_progress");
            }
            else if ([match.status isEqualToString:@"FINISHED"]){
                cell.lblStatus.text= NSLocalizedString(@"finished", @"finished");
            }
            
        }
        return cell;
    }
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"liveDetailsSegue"])
    {
        // Get reference to the destination view controller
        LiveDetailsViewController *vc = [segue destinationViewController];
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        // Pass any objects to the view controller here, like...
        [vc setMatch:[self.matchList.matchs objectAtIndex: indexPath.row]];
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    //test si l'utilisateur est loggué avant de permettre la transition vers l'autre VC
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.isLogged){
        return TRUE;
    }
    else{
        // si le match n'est pas commencé, les utilisateurs lambda ne doivent pas pouvoir voir les détails du matchs.
        // Si le lblstatus est vide, cela signifie que le match n'est pas commencé
        LiveViewCell *cell = (LiveViewCell*) sender;
        
        if ((cell.lblStatus == nil) || ([cell.lblStatus.text isEqualToString:@"Label"])){
            return FALSE;
        }
        else
            return TRUE;
    }
    
    return TRUE;
}


@end
