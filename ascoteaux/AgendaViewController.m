//
//  AgendaViewController.m
//  ascoteaux
//
//  Created by Arnaud on 29/09/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import "AgendaViewController.h"
#import "AFNetworking.h"
#import "Constant.h"
@interface AgendaViewController ()

@end

@implementation AgendaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"agenda", @"agenda");
    
    [self getResultsFromServer];
}


-(void) getResultsFromServer{
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0);
    [self.view addSubview: activityIndicator];
    
    [activityIndicator startAnimating];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/plain"];
    [manager GET:[BASE_URL stringByAppendingString: AGENDA_URL] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSString * htmlContent = [[responseObject objectForKey: @"payLoad"] objectForKey:@"htmlContent"];
        NSURL *mainBundleURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]];
        [self.webView loadHTMLString:htmlContent baseURL:mainBundleURL];
        [activityIndicator stopAnimating];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [activityIndicator stopAnimating];
    }];
    

}

@end
