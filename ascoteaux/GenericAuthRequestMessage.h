//
//  GenericAuthRequestMessage.h
//  ascoteaux
//
//  Created by Arnaud on 15/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONProtocole.h"

@interface GenericAuthRequestMessage : NSObject

@property(nonatomic,strong) NSDictionary *payload;
@property(nonatomic,strong) NSString *applicationVersion;
@property(nonatomic,strong) NSString *tokenId;
@property(nonatomic,strong) NSString *userId;

@end
