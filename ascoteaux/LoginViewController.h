//
//  LoginViewController.h
//  ascoteaux
//
//  Created by Arnaud on 28/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASCoteauxViewController.h"

@interface LoginViewController : ASCoteauxViewController

@property (nonatomic, strong) IBOutlet UITextField *tfLogin;
@property (nonatomic, strong) IBOutlet UITextField *tfPassword;
@property (nonatomic, strong) IBOutlet UIButton *btnValidate;

@end
