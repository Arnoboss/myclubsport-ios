//
//  NewsDetailsViewController.m
//  ascoteaux
//
//  Created by Arnaud on 21/09/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import "NewsDetailsViewController.h"
#import "ImageZoomViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation NewsDetailsViewController


- (void)viewDidLoad {

    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.title = NSLocalizedString(self.news.title, @"news");
    self.lblDate.text = [self.news getFormatedDate];
    self.lblTitle.text = [self.news.title uppercaseString];
    self.ivImage.image = nil; 
    
    [self.ivImage sd_setImageWithURL:[NSURL URLWithString:self.news.imageUrl]
                    placeholderImage:[UIImage imageNamed:self.news.imageUrl]];
    
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[self.news.content dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];

    self.lblContent.attributedText = attrStr;
    self.lblContent.font=[self.lblContent.font fontWithSize:15];
    
    [self.lblContent sizeToFit];
    
    self.ivImage.userInteractionEnabled = YES;
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapImageView:)];
    [self.ivImage addGestureRecognizer:recognizer];
    
}


- (void)onTapImageView:(UITapGestureRecognizer *)recognizer
{
    [self performSegueWithIdentifier:@"imageZoomSegue" sender:self];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"imageZoomSegue"])
    {
        // Get reference to the destination view controller
        ImageZoomViewController *vc = [segue destinationViewController];
        
        // Pass any objects to the view controller here, like...
        [vc setImage:self.ivImage.image];
    }
}

@end
