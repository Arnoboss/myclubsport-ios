//
//  UiLabelPadding.m
//  ascoteaux
//
//  Created by Arnaud on 26/09/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import "UiLabelPadding.h"

@implementation UiLabelPadding: UILabel

- (void)drawTextInRect:(CGRect)rect {
    UIEdgeInsets insets = {0, 12, 0, 10};
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}



@end
