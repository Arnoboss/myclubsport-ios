//
//  main.m
//  ascoteaux
//
//  Created by Arnaud on 02/09/2015.
//  Copyright (c) 2015 Arnaud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"


#ifndef DEBUG
#define NSLog(...)
#endif

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
