//
//  ASCoteauxViewController.h
//  ascoteaux
//
//  Created by Arnaud on 23/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Util.h"
@interface ASCoteauxViewController : UIViewController

- (void) displayErrorMessage:msg;

@end
