//
//  SecurityTokenMessage.m
//  ascoteaux
//
//  Created by Arnaud on 28/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import "SecurityTokenMessage.h"

@implementation SecurityTokenMessage

-(id) initWithDictionary: (NSDictionary *) dictionary{
    self = [super init];
    if (self) {
        self.tokenId = [dictionary objectForKey:@"tokenId"];
        self.expirationDate =[dictionary objectForKey:@"expirationDate"];
        self.active = [[dictionary objectForKey:@"active"] boolValue];
    }
    return self;
    
}


-(NSString *) convertToJSON{
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:self
                        options:NSJSONWritingPrettyPrinted
                        error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
    
}

- (NSMutableDictionary *)toNSDictionary{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    [dictionary setValue:self.tokenId
                  forKey:@"tokenId"];
    [dictionary setValue:self.expirationDate
                  forKey:@"expirationDate"];
    [dictionary setValue:[NSNumber numberWithBool:self.active] forKey:@"active"];
    
    return dictionary;
}

@end
