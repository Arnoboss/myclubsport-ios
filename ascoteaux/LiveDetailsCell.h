//
//  UILiveDetailsCell.h
//  ascoteaux
//
//  Created by Arnaud on 06/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LabelCellAutoSize.h"
@interface LiveDetailsCell : UITableViewCell

@property (nonatomic, strong) IBOutlet LabelCellAutoSize *lblTimeEvent;
@property (nonatomic, strong) IBOutlet LabelCellAutoSize *lblComment;

@end
