//
//  PagingStatsContentVC.h
//  ascoteaux
//
//  Created by Arnaud on 22/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayerListMessage.h"

@interface PagingStatsContentVC : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic) NSUInteger pageIndex;
@property (nonatomic, strong) NSString *teamTitle;

@property (nonatomic, strong) IBOutlet UILabel *lblTeam;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) PlayerListMessage *playerList;

@property (nonatomic) NSInteger sortType;

//Label mis en gras lors d'un tri
@property (nonatomic, strong) UILabel *lastClickedHeader;

- (void)userTappedOnHeader:(UIGestureRecognizer*)sender;
- (void) sortPlayersByTag: (NSInteger) viewTag andType: (NSInteger) sortType;

@end
