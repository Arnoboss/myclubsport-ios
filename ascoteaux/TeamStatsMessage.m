//
//  TeamStatsMessage.m
//  ascoteaux
//
//  Created by Arnaud on 23/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import "TeamStatsMessage.h"

@implementation TeamStatsMessage

-(id) initWithDictionary: (NSDictionary *) dictionary{
    self = [super init];
    if (self) {
        self.teamName = [dictionary objectForKey: @"teamName"];
        self.goals = [dictionary objectForKey: @"goals"];
        self.assists = [dictionary objectForKey: @"assists"];
        self.autogoals = [dictionary objectForKey: @"autogoals"];
        self.yellowCards = [dictionary objectForKey: @"yellowCards"];
        self.redCards = [dictionary objectForKey: @"redCards"];
        self.gamesPlayed = [dictionary objectForKey: @"gamesPlayed"];
        
    }
    return self;
}


/**
 Utilisé pour convertir un objet seul en Json. Pas testé mais devrait être fonctionnel si l'objet contient des objets de base (NSNumber, NSString, ...)
 */
-(NSString *) convertToJSON{
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
    
}

/**
 Convertit l'objet en dictionnaire. Obligatoire pour le convertir en JSON s'il est dans un Objet complexe (ex: NewsListMessage)
 */
- (NSMutableDictionary *)toNSDictionary
{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    [dictionary setValue:self.teamName forKey:@"teamName"];
    [dictionary setValue:self.goals forKey:@"goals"];
    [dictionary setValue:self.assists forKey:@"assists"];
    [dictionary setValue:self.autogoals forKey:@"autogoals"];
    [dictionary setValue:self.yellowCards forKey:@"yellowCards"];
    [dictionary setValue:self.redCards forKey:@"redCards"];
    [dictionary setValue:self.gamesPlayed forKey:@"gamesPlayed"];
    
    return dictionary;
}

//Constructeur recopie
-(id) initWithTeamStats: (TeamStatsMessage *) ts{
    self = [super init];
    if (self) {
        self.teamName=ts.teamName;
        self.goals=ts.goals;
        self.assists=ts.assists;
        self.autogoals=ts.autogoals;
        self.yellowCards=ts.yellowCards;
        self.redCards=ts.redCards;
        self.gamesPlayed=self.gamesPlayed;
    }
    
    return self;
}

//on initialize un nouveau teamStats en ajoutant les stats de 2 autres. utilisé pour le calcul des stats "Club"
-(id) initWithTeamStats: (TeamStatsMessage *) ts andAddingStats: (TeamStatsMessage *) ts2{
    self = [super init];
    if (self) {
        self.goals=[NSNumber numberWithInt:([ts.goals intValue] + [ts2.goals intValue])];
        self.assists=[NSNumber numberWithInt:([ts.assists intValue] + [ts2.assists intValue])];
        self.autogoals=[NSNumber numberWithInt:([ts.autogoals intValue] + [ts2.autogoals intValue])];
        self.yellowCards=[NSNumber numberWithInt:([ts.yellowCards intValue] + [ts2.yellowCards intValue])];
        self.redCards=[NSNumber numberWithInt:([ts.redCards intValue] + [ts2.redCards intValue])];
        self.gamesPlayed=[NSNumber numberWithInt:([ts.gamesPlayed intValue] + [ts2.gamesPlayed intValue])];
        
    }
    
    return self;
}
@end
