//
//  TeamStatsMessage.h
//  ascoteaux
//
//  Created by Arnaud on 23/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONProtocole.h"

@interface TeamStatsMessage : NSObject <JSONProtocole>

@property (nonatomic, strong) NSString *teamName;
@property (nonatomic, strong) NSNumber *goals;
@property (nonatomic, strong) NSNumber *assists;
@property (nonatomic, strong) NSNumber *autogoals;
@property (nonatomic, strong) NSNumber *yellowCards;
@property (nonatomic, strong) NSNumber *redCards;
@property (nonatomic, strong) NSNumber *gamesPlayed;

-(id) initWithTeamStats: (TeamStatsMessage *) ts;
-(id) initWithTeamStats: (TeamStatsMessage *) ts andAddingStats: (TeamStatsMessage *) ts2;
@end
