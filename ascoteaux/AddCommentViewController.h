//
//  AddCommentViewController.h
//  ascoteaux
//
//  Created by Arnaud on 08/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "CommentMessage.h"
#import "MatchMessage.h"

@interface AddCommentViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) MatchMessage * match;
@property(nonatomic, strong) CommentMessage *comment;
@property(nonatomic) BOOL isEditing;

@property(nonatomic, strong) IBOutlet UILabel *lblCommentPlaceHolder;

@property(nonatomic, strong) IBOutlet UILabel *lblTeam1;
@property(nonatomic, strong) IBOutlet UILabel *lblTeam2;
@property(nonatomic, strong) IBOutlet UITextField *lblGoalTeam1;
@property(nonatomic, strong) IBOutlet UITextField *lblGoalTeam2;
@property(nonatomic, strong) IBOutlet UITextField *lblButeursTeam1;
@property(nonatomic, strong) IBOutlet UITextField *lblButeursTeam2;
@property(nonatomic, strong) IBOutlet UITextField *lblMinuteEvent;
@property(nonatomic, strong) IBOutlet UITextView *tvComment;

@property(nonatomic, strong) IBOutlet UIPickerView *pvStatus;
@property(nonatomic, strong) IBOutlet UISwitch *swIsImportant;
@property(nonatomic, strong) IBOutlet UISwitch *swNotify;
@property(nonatomic, strong) IBOutlet UIButton *btnValidate;

@property (strong, nonatomic) NSArray *statusArray;

- (void) validate: (id) sender;

@end
