//
//  LiveDetailsViewController.m
//  ascoteaux
//
//  Created by Arnaud on 02/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import "LiveDetailsViewController.h"
#import "LiveDetailsCell.h"
#import "AFNetworking.h"
#import "Constant.h"
#import "CommentMessage.h"
#import "AddCommentViewController.h"
#import "Util.h"
#import "AppDelegate.h"

@implementation LiveDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    //initialisation de la navigation bar
    self.title = NSLocalizedString(@"liveDetails", @"liveDetails");
    
    //on initialise le "pull to refresh". Dans IB, activer la fonction "refresh" sur la tableview
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString: @"Tirer pour rafraichir"];
    [refreshControl addTarget:self action:@selector(getCommentsFromServer:) forControlEvents:UIControlEventValueChanged];
    [self.tableViewComments addSubview:refreshControl];
    
    //Ajout d'un bouton en haut à droite si l'utilisateur est loggué
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.isLogged){
        UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addComment:)];
        
        self.navigationItem.rightBarButtonItem = addButton;
    }
    
    // On remplit les champs
    self.lblButeursTeam1.text = [self.match buteursTeam1];
    self.lblButeursTeam2.text = [self.match buteursTeam2];
    self.lblDate.text = [self.match getFormatedDate];
    self.lblScore.text = [self.match getFormatedScore];
    self.lblTeam1.text = [self.match team1];
    self.lblTeam2.text = [self.match team2];
    
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self getCommentsFromServer: nil];
}

-(void) getCommentsFromServer: (UIRefreshControl *) refresh{
    
    UIActivityIndicatorView *activityIndicator;
    if (!refresh){
        activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0);
        [self.view addSubview: activityIndicator];
        
        [activityIndicator startAnimating];
    }
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/plain"];
    [manager GET:[BASE_URL stringByAppendingString: LIVE_MATCH_COMMENTS_URL] parameters:@{ @"matchId": self.match.id} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        self.match = [[MatchMessage alloc] initWithDictionary:[responseObject objectForKey: @"payLoad"]];
        if (refresh){
            [refresh endRefreshing];
        }
        else
            [activityIndicator stopAnimating];
        [self.tableViewComments reloadData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (refresh)
            [refresh endRefreshing];
        [activityIndicator stopAnimating];
        [Util displayPopupRequestProblem];
        NSLog(@"Error: %@", error);
    }];
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of sections.
    if (self.match.commentsList)
        return [[self.match.commentsList comments] count];
    else return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LiveDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"commentCellId" forIndexPath:indexPath];
    
    [self configureBasicCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self heightForBasicCellAtIndexPath:indexPath];
}

- (void)configureBasicCell:(LiveDetailsCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    CommentMessage *comment = [[CommentMessage alloc] init];
    comment =  [self.match.commentsList.comments objectAtIndex:indexPath.row];
    
    cell.lblTimeEvent.text = [NSString stringWithFormat:@"%@%@", comment.minuteEvent.stringValue , @"'"];
    cell.lblComment.text = comment.content;
    
    if (comment.important){
        cell.lblTimeEvent.font = [UIFont boldSystemFontOfSize:13.0f];
        cell.lblComment.font = [UIFont boldSystemFontOfSize:12.0f];
    }
    else{
        
        cell.lblTimeEvent.font = [UIFont systemFontOfSize:13.0f];
        cell.lblComment.font = [UIFont systemFontOfSize:12.0f];
    }
}

- (CGFloat)heightForBasicCellAtIndexPath:(NSIndexPath *)indexPath {
    static LiveDetailsCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.tableViewComments dequeueReusableCellWithIdentifier:@"commentCellId" ];
    });
    
    
    [self configureBasicCell:sizingCell atIndexPath:indexPath];
    return [self calculateHeightForConfiguredSizingCell:sizingCell];
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    sizingCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.tableViewComments.frame), CGRectGetHeight(sizingCell.bounds));
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + 1.0f; // Add 1.0f for the cell separator height
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get reference to the destination view controller
    AddCommentViewController *vc = [segue destinationViewController];
    [vc setMatch:self.match];
    
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"modifyCommentSegue"])
    {
        NSIndexPath *indexPath = [self.tableViewComments indexPathForSelectedRow];
        // Pass any objects to the view controller here, like...
        [vc setComment:[self.match.commentsList.comments objectAtIndex: indexPath.row]];
    }
}

- (void) addComment: (id) sender{
    [self performSegueWithIdentifier:@"addCommentSegue" sender:sender];
}


- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    //test si l'utilisateur est loggué avant de permettre la transition vers l'autre VC
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.isLogged){
        return TRUE;
    }
    else{
        return FALSE;
    }
}

//Désélection de la row après le clic
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableViewComments deselectRowAtIndexPath:[self.tableViewComments indexPathForSelectedRow] animated:YES];
}

@end
