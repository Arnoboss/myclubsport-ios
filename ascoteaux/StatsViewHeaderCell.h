//
//  StatsViewHeaderCell.h
//  ascoteaux
//
//  Created by Arnaud on 16/12/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatsViewHeaderCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *lblHeaderName;
@property (nonatomic, strong) IBOutlet UILabel *lblHeaderGamesPlayed;
@property (nonatomic, strong) IBOutlet UILabel *lblHeaderGoals;
@property (nonatomic, strong) IBOutlet UILabel *lblHeaderAssists;
@property (nonatomic, strong) IBOutlet UILabel *lblHeaderYellowCards;
@property (nonatomic, strong) IBOutlet UILabel *lblHeaderRedCards;

@end
