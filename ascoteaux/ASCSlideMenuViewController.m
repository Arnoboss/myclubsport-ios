//
//  ASCSlideMenuViewController.m
//  ascoteaux
//
//  Created by Arnaud on 13/09/2015.
//  Copyright (c) 2015 Arnaud. All rights reserved.
//

#import "ASCSlideMenuViewController.h"
#import "AppDelegate.h"
#import "Util.h"

@interface ASCSlideMenuViewController ()

@end

@implementation ASCSlideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (NSString *) segueIdentifierForIndexPathInLeftMenu:(NSIndexPath *)indexPath{
    
    NSString * identifier;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    switch (indexPath.row) {
        case 0:
            identifier=@"news";
            break;
        case 1:
            identifier=@"news";
            break;
        case 2:
            identifier=@"live";
            break;
        case 3:
            identifier=@"results";
            break;
        case 4:
            identifier =@"agenda";
            break;
        case 5:
            identifier =@"rank";
            break;
        case 6:
            identifier =@"calendar";
            break;
        case 7:
            identifier =@"stats";
            break;
        case 8:
            if (appDelegate.isLogged){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"disconnect","Déconnexion")
                                                                message:NSLocalizedString(@"confirmDisconnect","Êtes-vous sûr de vouloir vous déconnecter ?")
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"cancel","Annuler")
                                                      otherButtonTitles:NSLocalizedString(@"confirm","Valider"),nil];
                [alert show];
            }
            else{
                identifier =@"login";
            }
            break;
        default:
            break;
    }
    return identifier;
}



- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [Util disconnect];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *vc =[storyboard instantiateInitialViewController];
        [self presentViewController:vc animated:YES completion:nil];
        
    }
}



- (void) configureLeftMenuButton:(UIButton *)button{
    CGRect frame = button.frame;
    frame.origin = (CGPoint){0,0};
    frame.size = (CGSize){40,40};
    button.frame = frame;
    [button setImage: [UIImage imageNamed:@"icon_menu"] forState:UIControlStateNormal];
}

@end
