//
//  Constant.h
//  ascoteaux
//
//  Created by Arnaud on 09/09/2015.
//  Copyright (c) 2015 Arnaud. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Constant : NSObject

extern NSString* const BASE_URL;
extern NSString* const NOTIFICATIONS_URL;

extern NSString* const PLAYERS_URL ;
extern NSString* const PLAYER_URL;
extern NSString* const RANKING_URL ;
extern NSString* const RESULTS_URL ;
extern NSString* const CALENDAR_URL ;
extern NSString* const AGENDA_URL ;
extern NSString* const NEWS_URL ;
extern NSString* const LIVE_MATCH_URL ;
extern NSString* const LIVE_MATCH_COMMENTS_URL;
extern NSString* const LOGIN_URL ;


/**
 * PREFS alias
 */
extern NSString* const LOGIN;
extern NSString* const PASSWORD ;
extern NSString* const TOKEN_ID ;
extern NSString* const OLD_TOKEN_ID;
extern NSString* const TOKEN_EXPIRATION_DATE;
extern NSString* const NOTIFICATION_TOKEN;


+(NSString*) getCurrentSeason;


@end

