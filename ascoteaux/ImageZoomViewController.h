//
//  ImageZoomViewController.h
//  ascoteaux
//
//  Created by Arnaud on 27/09/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageZoomViewController : UIViewController

@property (nonatomic, strong)  IBOutlet UIImageView *ivImage;
@property (nonatomic, strong)  UIImage *image;

@end
