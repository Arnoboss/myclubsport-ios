//
//  NewsViewCell.h
//  ascoteaux
//
//  Created by Arnaud on 18/09/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *lblDate;
@property (nonatomic, strong) IBOutlet UIImageView *ivImage;
@property (nonatomic, strong) IBOutlet UILabel *lblTitle;

@end
