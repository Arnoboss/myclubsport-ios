//
//  PlayerListMessage.m
//  ascoteaux
//
//  Created by Arnaud on 23/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import "PlayerListMessage.h"
#import "PlayerMessage.h"

@implementation PlayerListMessage


-(id) initWithDictionary: (NSDictionary *) dictionary{
    self = [super init];
    if (self) {
        NSArray * playerListDict = [dictionary objectForKey: @"players"];
        NSMutableArray * mutableArray = [[NSMutableArray alloc] init];
        for (NSDictionary *current in playerListDict)
        {
            PlayerMessage *newsMsg= [[PlayerMessage alloc ] initWithDictionary:current];
            [mutableArray addObject:newsMsg];
        }
        
        self.players = [mutableArray copy];
    }
    return self;
    
}


-(NSString *) convertToJSON{
    NSMutableArray *newsData = [[NSMutableArray alloc] init];
    for (PlayerMessage *newsMsg in self.players) {
        [newsData addObject:[newsMsg toNSDictionary]];
    }
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:newsData
                        options:NSJSONWritingPrettyPrinted
                        error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
    
}

- (NSMutableDictionary *)toNSDictionary{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    NSMutableArray *playerList = [[NSMutableArray alloc] init];
    for(PlayerMessage *playerMsg in self.players){
        [playerList addObject:[playerMsg toNSDictionary]];
    }
    
    [dictionary setValue:playerList
                  forKey:@"players"];
    
    return dictionary;
}


-(void) addPLayerToList: (PlayerMessage *) pToAdd{
    if (! self.players){
        self.players= [[NSMutableArray alloc] init];
    }
    [self.players addObject:pToAdd];
}

@end
