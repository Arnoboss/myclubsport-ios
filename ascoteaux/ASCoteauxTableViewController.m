//
//  ASCoteauxTableViewController.m
//  ascoteaux
//
//  Created by Arnaud on 23/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import "ASCoteauxTableViewController.h"
#import "AppDelegate.h"
@interface ASCoteauxTableViewController ()

@end

@implementation ASCoteauxTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 7) {
        // iOS 7.0 or later
        self.navigationController.navigationBar.barTintColor = [UIColor redColor];
        self.navigationController.navigationBar.translucent = NO;
        self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    }else {
        // iOS 6.1 or earlier
        self.navigationController.navigationBar.tintColor = [UIColor redColor];
    }
    
    
    //on initialise le "pull to refresh". Dans IB, activer la fonction "refresh" sur la tableview
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString: @"Tirer pour rafraichir"];
    [refreshControl addTarget:self action:@selector(getDataFromServer:) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    
    //initialisation du navigationcontroller courant
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    appDelegate.currentNavController = self.navigationController;
    
}


-(void) getDataFromServer: (UIRefreshControl *) refresh{}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (void) displayErrorMessage:msg{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"requestErrorTitle", @"Erreur")
                                                    message:msg
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
}

@end
