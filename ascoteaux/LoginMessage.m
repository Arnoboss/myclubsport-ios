//
//  LoginMessage.m
//  ascoteaux
//
//  Created by Arnaud on 28/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import "LoginMessage.h"

@implementation LoginMessage

-(id) initWithDictionary: (NSDictionary *) dictionary{
    self = [super init];
    if (self) {
        self.login = [dictionary objectForKey:@"login"];
        self.password =[dictionary objectForKey:@"password"];
        self.token = [[SecurityTokenMessage alloc] initWithDictionary:[dictionary objectForKey:@"token"]];
    }
    return self;
    
}


-(NSString *) convertToJSON{
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:self
                        options:NSJSONWritingPrettyPrinted
                        error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
    
}

- (NSMutableDictionary *)toNSDictionary{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];

    [dictionary setValue:self.login
                  forKey:@"login"];
    [dictionary setValue:self.password
                  forKey:@"password"];
    [dictionary setValue:[self.token toNSDictionary]
                  forKey:@"token"];
    
    return dictionary;
}

@end
