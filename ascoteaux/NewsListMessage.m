//
//  NewsListMessage.m
//  ascoteaux
//
//  Created by Arnaud on 17/09/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import "NewsListMessage.h"
#import "NewsMessage.h"

@implementation NewsListMessage

-(id) initWithDictionary: (NSDictionary *) dictionary{
    self = [super init];
    if (self) {
        NSArray * newsListDict = [dictionary objectForKey: @"news"];
        NSMutableArray * mutableArray = [[NSMutableArray alloc] init];
        for (NSDictionary *current in newsListDict)
        {
            NewsMessage *newsMsg= [[NewsMessage alloc ] initWithDictionary:current];
            [mutableArray addObject:newsMsg];
        }
    
        self.news = [mutableArray copy];
    }
    return self;
    
}


-(NSString *) convertToJSON{
    NSMutableArray *newsData = [[NSMutableArray alloc] init];
    for (NewsMessage *newsMsg in self.news) {
        [newsData addObject:[newsMsg toNSDictionary]];
    }
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:newsData
                        options:NSJSONWritingPrettyPrinted
                        error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
    
}

- (NSMutableDictionary *)toNSDictionary{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    NSMutableArray *newsList = [[NSMutableArray alloc] init];
    for(NewsMessage *newsMsg in self.news){
        [newsList addObject:[newsMsg toNSDictionary]];
    }
    
    [dictionary setValue:newsList
                  forKey:@"news"];
    
    return dictionary;
}

@end
