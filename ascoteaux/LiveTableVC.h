//
//  LiveTableVC.h
//  ascoteaux
//
//  Created by Arnaud on 30/09/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MatchListMessage.h"
#import "ASCoteauxTableViewController.h"

@interface LiveTableVC : ASCoteauxTableViewController

@property (nonatomic, strong) MatchListMessage * matchList;
@property (nonatomic) BOOL requestDone;
@end
