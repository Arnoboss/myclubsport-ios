//
//  MatchMessage.m
//  ascoteaux
//
//  Created by Arnaud on 30/09/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import "MatchMessage.h"

@implementation MatchMessage

-(id) initWithDictionary: (NSDictionary *) dictionary{
    self = [super init];
    if (self) {
        self.id = [dictionary objectForKey: @"id"];
        NSTimeInterval timestamp = (NSTimeInterval)[[dictionary objectForKey: @"date"] doubleValue];
        
        self.date = [NSDate dateWithTimeIntervalSince1970:timestamp/1000];
        self.team1 = [dictionary objectForKey: @"team1"];
        self.team2 = [dictionary objectForKey: @"team2"];
        self.goalsTeam1 = [dictionary objectForKey: @"goalsTeam1"];
        self.goalsTeam2 = [dictionary objectForKey: @"goalsTeam2"];
        self.buteursTeam1 = [dictionary objectForKey: @"buteursTeam1"];
        self.buteursTeam2 = [dictionary objectForKey: @"buteursTeam2"];
        self.intitule = [dictionary objectForKey: @"intitule"];
        self.commentsList = [[CommentListMessage alloc] initWithArray:[dictionary objectForKey: @"comments"]];
        self.status = [dictionary objectForKey: @"status"];
        
    }
    return self;
}


/**
 Utilisé pour convertir un objet seul en Json. Pas testé mais devrait être fonctionnel si l'objet contient des objets de base (NSNumber, NSString, ...)
 */
-(NSString *) convertToJSON{
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
    
}

/**
 Convertit l'objet en dictionnaire. Obligatoire pour le convertir en JSON s'il est dans un Objet complexe (ex: NewsListMessage)
 */
- (NSMutableDictionary *)toNSDictionary
{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setValue:self.id forKey:@"id"];
    [dictionary setValue:[NSNumber numberWithDouble:[self.date timeIntervalSince1970] * 1000] forKey:@"date"];
    [dictionary setValue:self.team1 forKey:@"team1"];
    [dictionary setValue:self.team2 forKey:@"team2"];
    [dictionary setValue:self.goalsTeam1 forKey:@"goalsTeam1"];
    [dictionary setValue:self.goalsTeam2 forKey:@"goalsTeam2"];
    [dictionary setValue:self.buteursTeam1 forKey:@"buteursTeam1"];
    [dictionary setValue:self.buteursTeam2 forKey:@"buteursTeam2"];
    [dictionary setValue:self.intitule forKey:@"intitule"];
    [dictionary setValue:self.status forKey:@"status"];
    
    NSMutableArray *comments = [[NSMutableArray alloc] init];
    for ( id comment in self.commentsList.comments ){
        [comments addObject:[((CommentMessage *) comment) toNSDictionary]];
    }
    
    [dictionary setObject:comments forKey:@"comments"];
    
    return dictionary;
}

- (NSString *) getFormatedDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSLocale *frLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"fr_FR"];
    [formatter setLocale:frLocale];
    
    [formatter setDateStyle:NSDateFormatterLongStyle];
    [formatter setTimeStyle:NSDateFormatterNoStyle];
    
    return [formatter stringFromDate:self.date];
}

- (NSString *) getStartMatchTime
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"HH:mm"];
    return [formatter stringFromDate:self.date];
}


- (NSString *) getFormatedScore{
    return [NSString stringWithFormat:@"%@%@%@", self.goalsTeam1 , @" - ", self.goalsTeam2];
}


@end
