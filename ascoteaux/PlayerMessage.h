//
//  PlayerMessage.h
//  ascoteaux
//
//  Created by Arnaud on 23/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONProtocole.h"
#import "TeamStatsMessage.h"

@interface PlayerMessage : NSObject <JSONProtocole>

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *position;
@property (nonatomic, strong) NSNumber *age;
@property (nonatomic, strong) NSMutableArray *teamStats;

- (id) initWithPlayerMessage: (PlayerMessage *) playerMsg andTeamStats: (TeamStatsMessage *) teamStats;
- (void) totalTeamStats: (TeamStatsMessage *) ts;
- (NSString *) getFormatedInfos;
@end
