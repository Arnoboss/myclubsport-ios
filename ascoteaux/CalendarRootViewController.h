//
//  CalendarRootViewController.h
//  ascoteaux
//
//  Created by Arnaud on 21/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PagingCalendarContentVC.h"
#import "ASCoteauxViewController.h"

@interface CalendarRootViewController : ASCoteauxViewController <UIPageViewControllerDataSource>


@property (nonatomic,strong) UIPageViewController *pageViewController;
@property (nonatomic,strong) NSArray *arrPageTitles;
@property (strong, nonatomic) NSArray *arrPageUrls;
@end
