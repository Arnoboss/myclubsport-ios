//
//  NotificationIdMessage.m
//  ascoteaux
//
//  Created by Arnaud on 04/11/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import "NotificationIdMessage.h"

@implementation NotificationIdMessage

-(id) initWithDictionary: (NSDictionary *) dictionary{
    self = [super init];
    if (self) {
        self.notificationId = [dictionary objectForKey:@"notificationId"];
    }
    return self;
    
}


-(NSString *) convertToJSON{
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:self
                        options:NSJSONWritingPrettyPrinted
                        error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
    
}

- (NSMutableDictionary *)toNSDictionary{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    [dictionary setValue:self.notificationId
                  forKey:@"notificationId"];
    
    return dictionary;
}


@end
