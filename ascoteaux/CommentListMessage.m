//
//  CommentListMessage.m
//  ascoteaux
//
//  Created by Arnaud on 30/09/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import "CommentListMessage.h"
#import "CommentMessage.h"

@implementation CommentListMessage

-(id) initWithDictionary: (NSDictionary *) dictionary{
    return [self initWithArray:[dictionary objectForKey: @"comments"]];
}


-(id) initWithArray: (NSArray *) commentListArray{
    self = [super init];
    if (self) {
        NSMutableArray * mutableArray = [[NSMutableArray alloc] init];
        for (NSDictionary *current in commentListArray)
        {
            CommentMessage *commentsMsg= [[CommentMessage alloc ] initWithDictionary:current];
            [mutableArray addObject:commentsMsg];
        }
        
        self.comments = [mutableArray copy];
    }
    return self;
}


-(id) initWithComment: (CommentMessage *) comment{
    self = [super init];
    NSArray *array= [[NSArray alloc] initWithObjects:comment, nil];
    self.comments = array;
    return self;
}


-(NSString *) convertToJSON{
    NSMutableArray *commentsData = [[NSMutableArray alloc] init];
    for (CommentMessage *commentsMsg in self.comments) {
        [commentsData addObject:[commentsMsg toNSDictionary]];
    }
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:commentsData
                        options:NSJSONWritingPrettyPrinted
                        error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
    
}

- (NSMutableDictionary *)toNSDictionary{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    NSMutableArray *commentsList = [[NSMutableArray alloc] init];
    for(CommentMessage *commentsMsg in self.comments){
        [commentsList addObject:[commentsMsg toNSDictionary]];
    }
    
    [dictionary setValue:commentsList
                  forKey:@"comments"];
    
    return dictionary;
}

@end
