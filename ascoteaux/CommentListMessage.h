//
//  CommentListMessage.h
//  ascoteaux
//
//  Created by Arnaud on 30/09/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONProtocole.h"
#import "CommentMessage.h"

@interface CommentListMessage : NSObject <JSONProtocole>
@property (nonatomic, strong) NSArray * comments;
-(id) initWithArray: (NSArray *) commentListArray;
-(id) initWithComment: (CommentMessage *) comment;
@end
