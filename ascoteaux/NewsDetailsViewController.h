//
//  NewsDetailsViewController.h
//  ascoteaux
//
//  Created by Arnaud on 21/09/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsMessage.h"
#import "ASCoteauxViewController.h"

@interface NewsDetailsViewController : ASCoteauxViewController

@property (nonatomic, strong) NewsMessage * news;
@property (nonatomic, strong) IBOutlet UIImageView *ivImage;
@property (nonatomic, strong) IBOutlet UILabel *lblTitle;
@property (nonatomic, strong) IBOutlet UILabel *lblDate;
@property (nonatomic, strong) IBOutlet UILabel *lblContent;

@end
