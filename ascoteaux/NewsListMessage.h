//
//  NewsListMessage.h
//  ascoteaux
//
//  Created by Arnaud on 17/09/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONProtocole.h"

@interface NewsListMessage : NSObject <JSONProtocole>

@property (nonatomic, strong) NSArray * news;


@end
