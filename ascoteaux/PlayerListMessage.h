//
//  PlayerListMessage.h
//  ascoteaux
//
//  Created by Arnaud on 23/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONProtocole.h"
#import "PlayerMessage.h"

@interface PlayerListMessage : NSObject <JSONProtocole>

@property (nonatomic, strong) NSMutableArray *players;

-(void) addPLayerToList: (PlayerMessage *) pToAdd;
@end
