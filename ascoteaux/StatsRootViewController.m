//
//  StatsRootViewController.m
//  ascoteaux
//
//  Created by Arnaud on 22/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import "StatsRootViewController.h"
#import "PagingStatsContentVC.h"
#import "AFNetworking.h"
#import "Constant.h"
#import "PlayerMessage.h"

@interface StatsRootViewController ()

@end

@implementation StatsRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"stats", @"stats");
    
    self.arrPageTitles = @[@"Club",@"Equipe A",@"Equipe B",@"Equipe C"];
    
    
    // Create page view controller
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewControllerStats"];
    self.pageViewController.dataSource = self;
    
    PagingStatsContentVC *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height );//- 5);
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
    [self getPlayerStatsFromServer];
}


-(void) getPlayerStatsFromServer{
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0);
    [self.view addSubview: activityIndicator];
    
    [activityIndicator startAnimating];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/plain"];
    [manager GET: [NSString stringWithFormat:@"%@%@%@%@",BASE_URL, PLAYERS_URL, @"?season=", Constant.getCurrentSeason] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        PlayerListMessage *players = [[PlayerListMessage alloc] initWithDictionary:[responseObject objectForKey: @"payLoad"]];
        
        [self dispatchPlayerListByTeam: players];
        
        PagingStatsContentVC *startingViewController = [self viewControllerAtIndex:0];
        NSArray *viewControllers = @[startingViewController];
        [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        
        [activityIndicator stopAnimating];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [activityIndicator stopAnimating];
        [Util displayPopupRequestProblem];
        NSLog(@"Error: %@", error);
    }];
    
}

//On divise la liste en 4 pour qu'elle soit triée par équipe et qu'elle soit plus lisible.
-(void) dispatchPlayerListByTeam: (PlayerListMessage *) pListTotal{
    self.pListA= [[PlayerListMessage alloc] init];
    self.pListB= [[PlayerListMessage alloc] init];
    self.pListC= [[PlayerListMessage alloc] init];
    self.pListClub= [[PlayerListMessage alloc] init];
    for (PlayerMessage *p in pListTotal.players){
        PlayerMessage *pClub=[[PlayerMessage alloc]initWithPlayerMessage:p andTeamStats:nil];
        
        for (TeamStatsMessage *ts in p.teamStats ){
            if ([ts.gamesPlayed intValue]>0){
                PlayerMessage *pToAdd= [[PlayerMessage alloc]initWithPlayerMessage:p andTeamStats:ts];
                
                if([ts.teamName isEqualToString:@"A"]){
                    [self.pListA addPLayerToList:pToAdd];
                }
                else if([ts.teamName isEqualToString:@"B"]){
                    [self.pListB addPLayerToList:pToAdd];
                }
                else if([ts.teamName isEqualToString:@"C"]){
                    [self.pListC addPLayerToList:pToAdd];
                }
                
                [pClub totalTeamStats: ts];
            }
        }
        
        [self.pListClub addPLayerToList :pClub];
    }
    
}

- (PagingStatsContentVC *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.arrPageTitles count] == 0) || (index >= [self.arrPageTitles count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    PagingStatsContentVC *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentViewControllerStats"];
    pageContentViewController.teamTitle = self.arrPageTitles[index];
    pageContentViewController.pageIndex = index;
    if (index==0)
        pageContentViewController.playerList = self.pListClub;
    else if (index==1)
        pageContentViewController.playerList = self.pListA;
    
    else if (index==2)
        pageContentViewController.playerList = self.pListB;
    
    else if (index==3)
        pageContentViewController.playerList = self.pListC;
    
    return pageContentViewController;
}


#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PagingStatsContentVC*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PagingStatsContentVC*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.arrPageTitles count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.arrPageTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

- (void) handleTapGesture: (UIGestureRecognizer*) recognizer{
}

@end
