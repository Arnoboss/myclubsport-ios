//
//  RankRootViewController.h
//  ascoteaux
//
//  Created by Arnaud on 20/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PagingRankContentVC.h"
#import "ASCoteauxViewController.h"

@interface RankRootViewController : ASCoteauxViewController <UIPageViewControllerDataSource>


@property (nonatomic,strong) UIPageViewController *pageViewController;
@property (nonatomic,strong) NSArray *arrPageTitles;
@property (strong, nonatomic) NSArray *arrPageUrls;


@end
