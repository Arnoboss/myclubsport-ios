//
//  StatsRootViewController.h
//  ascoteaux
//
//  Created by Arnaud on 22/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASCoteauxViewController.h"
#import "PlayerListMessage.h"

@interface StatsRootViewController : ASCoteauxViewController<UIPageViewControllerDataSource>

@property (nonatomic,strong) UIPageViewController *pageViewController;
@property (nonatomic,strong) NSArray *arrPageTitles;
@property (nonatomic,strong) PlayerListMessage *pListClub;
@property (nonatomic,strong) PlayerListMessage *pListA;
@property (nonatomic,strong) PlayerListMessage *pListB;
@property (nonatomic,strong) PlayerListMessage *pListC;

@end
