//
//  LoginMessage.h
//  ascoteaux
//
//  Created by Arnaud on 28/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SecurityTokenMessage.h"
#import "JSONProtocole.h"

@interface LoginMessage : NSObject <JSONProtocole>

@property (nonatomic, strong) NSString *login;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) SecurityTokenMessage *token;

@end
