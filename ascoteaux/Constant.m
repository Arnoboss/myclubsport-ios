//
//  Constant.m
//  ascoteaux
//
//  Created by Arnaud on 09/09/2015.
//  Copyright (c) 2015 Arnaud. All rights reserved.
//

#import "Constant.h"

@interface Constant ()

@end

@implementation Constant



//NSString* const BASE_URL = @"http://ascoteaux.appspot.com";
//NSString* const BASE_URL = @"http://1-dot-ascoteaux.appspot.com";
NSString* const BASE_URL = @"http://ascoteauxdev.appspot.com";
//NSString* const BASE_URL = @"http://192.168.1.17:8888";


NSString* const NOTIFICATIONS_URL = @"/rest/notifications";
NSString* const PLAYERS_URL = @"/rest/players";
NSString* const PLAYER_URL  = @"/rest/player";
NSString* const RANKING_URL = @"/rest/ranking?team=";
NSString* const RESULTS_URL = @"/rest/results";
NSString* const CALENDAR_URL  = @"/rest/calendar?team=";
NSString* const AGENDA_URL  = @"/rest/agenda";
NSString* const NEWS_URL  = @"/rest/news";
NSString* const LIVE_MATCH_URL  = @"/rest/liveMatch";
NSString* const LIVE_MATCH_COMMENTS_URL  = @"/rest/liveMatchComments";
NSString* const LOGIN_URL  = @"/rest/login";


/**
 * PREFS alias
 */
NSString* const LOGIN = @"userId";
NSString* const PASSWORD = @"password";
NSString* const TOKEN_ID = @"tokenId";
NSString* const OLD_TOKEN_ID = @"oldTokenId";
NSString* const TOKEN_EXPIRATION_DATE = @"tokenExpirationDate";
NSString* const NOTIFICATION_TOKEN=@"notificationToken";

+(NSString*) getCurrentSeason{
    NSDate *now = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:now];

    NSInteger month = [components month];
    NSInteger year = [components year];
    
    if (month<8)
        return [NSString stringWithFormat:@"%ld-%ld", (long)year-1, (long) year];
    else
        return [NSString stringWithFormat:@"%ld-%ld", (long)year, (long) year+1];
}


@end