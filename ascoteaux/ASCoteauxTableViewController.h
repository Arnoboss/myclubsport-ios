//
//  ASCoteauxTableViewController.h
//  ascoteaux
//
//  Created by Arnaud on 23/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Util.h"

@interface ASCoteauxTableViewController : UITableViewController

-(void) getDataFromServer: (UIRefreshControl *) refresh;

- (void) displayErrorMessage:msg;

@end
