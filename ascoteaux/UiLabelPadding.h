//
//  UiLabelPadding.h
//  ascoteaux
//
//  Created by Arnaud on 26/09/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UiLabelPadding : UILabel

@property (nonatomic, assign) UIEdgeInsets edgeInsets;
@end
