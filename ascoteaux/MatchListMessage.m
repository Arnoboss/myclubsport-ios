//
//  MatchListMessage.m
//  ascoteaux
//
//  Created by Arnaud on 30/09/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import "MatchListMessage.h"
#import "MatchMessage.h"

@implementation MatchListMessage

-(id) initWithDictionary: (NSDictionary *) dictionary{
    self = [super init];
    if (self) {
        NSArray * matchsListDict = [dictionary objectForKey: @"matchs"];
        NSMutableArray * mutableArray = [[NSMutableArray alloc] init];
        for (NSDictionary *current in matchsListDict)
        {
            MatchMessage *matchsMsg= [[MatchMessage alloc ] initWithDictionary:current];
            [mutableArray addObject:matchsMsg];
        }
        
        self.matchs = [mutableArray copy];
    }
    return self;
    
}


-(NSString *) convertToJSON{
    NSMutableArray *matchsData = [[NSMutableArray alloc] init];
    for (MatchMessage *matchsMsg in self.matchs) {
        [matchsData addObject:[matchsMsg toNSDictionary]];
    }
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:matchsData
                        options:NSJSONWritingPrettyPrinted
                        error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
    
}

- (NSMutableDictionary *)toNSDictionary{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    NSMutableArray *matchsList = [[NSMutableArray alloc] init];
    for(MatchMessage *matchsMsg in self.matchs){
        [matchsList addObject:[matchsMsg toNSDictionary]];
    }
    
    [dictionary setValue:matchsList
                  forKey:@"matchs"];
    
    return dictionary;
}


@end
