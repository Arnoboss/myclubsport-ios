//
//  PagingStatsContentVC.m
//  ascoteaux
//
//  Created by Arnaud on 22/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import "PagingStatsContentVC.h"
#import "StatsViewCell.h"
#import "StatsViewHeaderCell.h"

@interface PagingStatsContentVC ()

@end

@implementation PagingStatsContentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.lblTeam.text=self.teamTitle;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of sections.
    if (self.playerList.players)
        return [self.playerList.players count]+1;
    else return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row==0){
        StatsViewHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"statsCellHeader" forIndexPath:indexPath];
        [self configureHeaderCell:cell atIndexPath:indexPath];
        return cell;
    }
    StatsViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"statsCellId" forIndexPath:indexPath];
    
    indexPath = [NSIndexPath indexPathForRow:indexPath.row-1 inSection:indexPath.section];
    [self configureBasicCell:cell atIndexPath:indexPath];
    
    return cell;
}


- (void)configureHeaderCell:(StatsViewHeaderCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    // We catch the user tap on the header
    [cell.lblHeaderGamesPlayed addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedOnHeader:)]];
    [cell.lblHeaderName addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedOnHeader:)]];
    [cell.lblHeaderAssists addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedOnHeader:)]];
    [cell.lblHeaderGoals addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedOnHeader:)]];
    [cell.lblHeaderRedCards addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedOnHeader:)]];
    [cell.lblHeaderYellowCards addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedOnHeader:)]];
    
    if (self.lastClickedHeader==nil){
        self.lastClickedHeader = cell.lblHeaderName;
        self.lastClickedHeader.font = [UIFont boldSystemFontOfSize:19.0f];
    }
}


- (void)configureBasicCell:(StatsViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    PlayerMessage *player = [[PlayerMessage alloc] init];
    player =  [self.playerList.players objectAtIndex:indexPath.row];
    
    cell.lblName.text = player.name;
    cell.lblPlayerInfos.text = [player getFormatedInfos];
    
    TeamStatsMessage *ts = [player.teamStats objectAtIndex:0];
    cell.lblGoals.text = [ts.goals stringValue];
    cell.lblGamesPlayed.text = [ts.gamesPlayed stringValue];
    cell.lblAssists.text = [ts.assists stringValue];
    cell.lblYellowCards.text = [ts.yellowCards stringValue];
    cell.lblRedCards.text = [ts.redCards stringValue];
    
}

- (void)userTappedOnHeader:(UIGestureRecognizer*)sender{
    self.lastClickedHeader.font = [UIFont systemFontOfSize:18.0f];
    UILabel *clickedLabel = (UILabel *) sender.view;
    clickedLabel.font = [UIFont boldSystemFontOfSize:19.0f];
    if (self.lastClickedHeader == clickedLabel){
        if (self.sortType==0){
            self.sortType = (NSInteger) 1;
        }
        else{
            self.sortType = (NSInteger) 0;
        }
            [self sortPlayersByTag: clickedLabel.tag andType:self.sortType];
        
    }
    else{
        self.lastClickedHeader = clickedLabel;
        [self sortPlayersByTag: clickedLabel.tag andType:0];
    }
    
}

// sortBy: 0 = ASC, 1 = DESC
- (void) sortPlayersByTag: (NSInteger) viewTag andType: (NSInteger) sortType {
    NSArray *sortedArray;
    switch (viewTag) {
        case 0:
            if (sortType==0){
                sortedArray = [self.playerList.players sortedArrayUsingComparator:^NSComparisonResult(PlayerMessage *p1, PlayerMessage *p2){
                    return [p1.name compare:p2.name];
                }];
            }
            else{
                sortedArray = [self.playerList.players sortedArrayUsingComparator:^NSComparisonResult(PlayerMessage *p1, PlayerMessage *p2){
                    return [p2.name compare:p1.name];
                }];
            }
            break;
        case 1:
            if (sortType==0){
                sortedArray = [self.playerList.players sortedArrayUsingComparator:^NSComparisonResult(PlayerMessage *p1, PlayerMessage *p2){
                    return [((TeamStatsMessage*)p2.teamStats.firstObject).gamesPlayed compare:((TeamStatsMessage*)p1.teamStats.firstObject).gamesPlayed ];
                }];
            }
            else{
                sortedArray = [self.playerList.players sortedArrayUsingComparator:^NSComparisonResult(PlayerMessage *p1, PlayerMessage *p2){
                    return [((TeamStatsMessage*)p1.teamStats.firstObject).gamesPlayed compare:((TeamStatsMessage*)p2.teamStats.firstObject).gamesPlayed ];
                }];
            }
            break;
        case 2:
            if (sortType==0){
                sortedArray = [self.playerList.players sortedArrayUsingComparator:^NSComparisonResult(PlayerMessage *p1, PlayerMessage *p2){
                    return [((TeamStatsMessage*)p2.teamStats.firstObject).goals compare:((TeamStatsMessage*)p1.teamStats.firstObject).goals ];
                }];
            }
            else{
                sortedArray = [self.playerList.players sortedArrayUsingComparator:^NSComparisonResult(PlayerMessage *p1, PlayerMessage *p2){
                    return [((TeamStatsMessage*)p1.teamStats.firstObject).goals compare:((TeamStatsMessage*)p2.teamStats.firstObject).goals ];
                }];
            }
            break;
        case 3:
            if (sortType==0){
                sortedArray = [self.playerList.players sortedArrayUsingComparator:^NSComparisonResult(PlayerMessage *p1, PlayerMessage *p2){
                    return [((TeamStatsMessage*)p2.teamStats.firstObject).assists compare:((TeamStatsMessage*)p1.teamStats.firstObject).assists ];
                }];
            }
            else{
                sortedArray = [self.playerList.players sortedArrayUsingComparator:^NSComparisonResult(PlayerMessage *p1, PlayerMessage *p2){
                    return [((TeamStatsMessage*)p1.teamStats.firstObject).goals compare:((TeamStatsMessage*)p2.teamStats.firstObject).goals ];
                }];
            }
            break;
        case 4:
            if (sortType==0){
                sortedArray = [self.playerList.players sortedArrayUsingComparator:^NSComparisonResult(PlayerMessage *p1, PlayerMessage *p2){
                    return [((TeamStatsMessage*)p2.teamStats.firstObject).yellowCards compare:((TeamStatsMessage*)p1.teamStats.firstObject).yellowCards ];
                }];
            }
            else{
                sortedArray = [self.playerList.players sortedArrayUsingComparator:^NSComparisonResult(PlayerMessage *p1, PlayerMessage *p2){
                    return [((TeamStatsMessage*)p1.teamStats.firstObject).yellowCards compare:((TeamStatsMessage*)p2.teamStats.firstObject).yellowCards ];
                }];
            }
            break;
        case 5:
            if (sortType==0){
                sortedArray = [self.playerList.players sortedArrayUsingComparator:^NSComparisonResult(PlayerMessage *p1, PlayerMessage *p2){
                    return [((TeamStatsMessage*)p2.teamStats.firstObject).redCards compare:((TeamStatsMessage*)p1.teamStats.firstObject).redCards ];
                }];
            }
            else {
                sortedArray = [self.playerList.players sortedArrayUsingComparator:^NSComparisonResult(PlayerMessage *p1, PlayerMessage *p2){
                    return [((TeamStatsMessage*)p1.teamStats.firstObject).redCards compare:((TeamStatsMessage*)p2.teamStats.firstObject).redCards ];
                }];
            }
            break;
            
        default:
            break;
    }
    self.playerList.players=[NSMutableArray arrayWithArray:sortedArray];
    [_tableView reloadData];
    
}

@end
