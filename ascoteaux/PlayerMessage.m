//
//  PlayerMessage.m
//  ascoteaux
//
//  Created by Arnaud on 23/10/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import "PlayerMessage.h"

@implementation PlayerMessage

-(id) initWithDictionary: (NSDictionary *) dictionary{
    self = [super init];
    if (self) {
        self.name = [dictionary objectForKey: @"name"];
        self.position = [dictionary objectForKey: @"position"];
        self.age = [dictionary objectForKey: @"age"];
        
        NSMutableArray * mutableArray = [[NSMutableArray alloc] init];
        for (NSDictionary *current in [dictionary objectForKey: @"teamStats"])
        {
            TeamStatsMessage *tsMsg= [[TeamStatsMessage alloc ] initWithDictionary:current];
            [mutableArray addObject:tsMsg];
        }
        
        self.teamStats = [mutableArray copy];
        
    }
    return self;
}


/**
 Utilisé pour convertir un objet seul en Json. Pas testé mais devrait être fonctionnel si l'objet contient des objets de base (NSNumber, NSString, ...)
 */
-(NSString *) convertToJSON{
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
    
}

/**
 Convertit l'objet en dictionnaire. Obligatoire pour le convertir en JSON s'il est dans un Objet complexe (ex: NewsListMessage)
 */
- (NSMutableDictionary *)toNSDictionary
{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setValue:self.name forKey:@"name"];
    [dictionary setValue:self.position forKey:@"position"];
    [dictionary setValue:self.age forKey:@"age"];
    
    [dictionary setObject:self.teamStats forKey:@"teamStats"];
    
    return dictionary;
}


- (id) initWithPlayerMessage: (PlayerMessage *) playerMsg andTeamStats: (TeamStatsMessage *) teamStats{
    self.age = playerMsg.age;
    self.position=playerMsg.position;
    self.name= playerMsg.name;
    
    if (teamStats){
        self.teamStats = [[NSMutableArray alloc] initWithObjects:teamStats, nil];
    }
    
    return self;
}


// calcule le total des stats fournies en parametre pour creer une seule grosse stat
- (void) totalTeamStats: (TeamStatsMessage *) ts{
    if ((self.teamStats == nil) || [self.teamStats count] <1){
        self.teamStats = [[NSMutableArray alloc] initWithObjects:ts, nil];
    }
    else{
        TeamStatsMessage *oldTs= [self.teamStats objectAtIndex:0];
        TeamStatsMessage *newTs= [[TeamStatsMessage alloc] initWithTeamStats:oldTs andAddingStats:ts];
        [self.teamStats insertObject:newTs atIndex:0];
    }
}

- (NSString *) getFormatedInfos{
    return [NSString stringWithFormat:@"%@ - %@ %@" ,self.position, self.age, NSLocalizedString(@"yearsold", @"ans")];
}

@end
