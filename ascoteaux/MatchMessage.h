//
//  MatchMessage.h
//  ascoteaux
//
//  Created by Arnaud on 30/09/2015.
//  Copyright © 2015 Arnaud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONProtocole.h"
#import "CommentListMessage.h"

@interface MatchMessage : NSObject <JSONProtocole>

typedef enum {
    NOT_STARTED, IN_PROGRESS, FIRST_HALF, HALF_TIME, SECOND_HALF, FINISHED, REPORTED
} MatchStatus;

@property (nonatomic, strong) NSNumber *id;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSString *team1;
@property (nonatomic, strong) NSString *team2;
@property (nonatomic, strong) NSNumber *goalsTeam1;
@property (nonatomic, strong) NSNumber *goalsTeam2;
@property (nonatomic, strong) NSString *buteursTeam1;
@property (nonatomic, strong) NSString *buteursTeam2;
@property (nonatomic, strong) NSString *intitule;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) CommentListMessage *commentsList;

- (NSString *) getFormatedDate;
- (NSString *) getStartMatchTime;
- (NSString *) getFormatedScore;

@end
