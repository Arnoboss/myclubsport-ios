# README #

### AS Coteaux iOS application ###

* This project is AS Coteaux iOS Application. It works with the web-application located at http://ascoteaux.appspot.com
* Version 1.0

### Summary ###

* Works with Xcode 7, min target iOS 8.0
* Uses Pods for dependencies
* Dependencies: AMSlideMenu


### Licence ###

* 2015, Arnaud Bossmann, Apache licence